import axios from "../plugins/axios";
import {UserModel} from '../models/UserModel';

const state = {
  currentUser: {},
  groups: [],
  permissions: [],
  users: [],
};

const getters = {
  getGroups: state => state.groups,
  getUsers: state => state.users,
  getCurrentUser: state => state.currentUser,
  getGroupById: (state) => {
    return (id) => state.groups.find(gr => gr.id === parseInt(id))
  },
  getContentPermissions: state => (id) => {
    const group = state.groups.find(gr => gr.id === parseInt(id));
    const list = [];
    if (group) {
      let contentTypeId = null;
      let temp = Object.assign({}, {
        name: '',
        permissions: []
      });
      const setContentPerms = (perm) => {
        const codename = perm.codename.split('_');
        temp.name = codename[1];
        temp.permissions.push({
          id: perm.id,
          action: codename[0],
          isChecked: group.permissions.includes(perm.id)
        });
      };
      state.permissions.map((perm, i) => {
        if (i === 0) {
          contentTypeId = perm.content_type_id
        } // инициализируем contentTypeId
        if (contentTypeId === perm.content_type_id) {
          setContentPerms(perm);
          if (i === (state.permissions.length - 1)) {
            list.push(temp)
          }
        } else {
          list.push(temp);
          temp = Object.assign({}, {
            name: '',
            permissions: []
          });
          contentTypeId = perm.content_type_id;
          setContentPerms(perm);
        }
      });
    }
    return list;
  },
  getUserById: (state) => {
    return (id) => state.users.find(user => user.id === id)
  },
  getUserGroups: state => (id) => {
    const user = state.users.find(user => user.id === id);
    const groups = state.groups;
    if (user) {
      groups.forEach(group => {
        group.isChecked = user.groups.includes(group.id);
      });
    }
    return groups;
  },
};

const actions = {
  fetchGroups({commit}) {
    axios.get('/api/groups-permissions-list/')
      .then(response => {
        const data = response.data;
        commit('GROUP_PERMISSIONS_LIST', data.info);
      })
      .catch(error => console.log(error));
  },
  fetchUsers({commit}) {
    axios.get('/api/users/')
      .then(response => {
        const data = response.data;
        commit('USER_LIST', data);
      })
      .catch(error => console.log(error));
  },
  groupSave({commit}, payload) {
    return new Promise((resolve, reject) => {
      const method = payload.id ? 'put' : 'post';
      axios[method]('/api/group-save/', {
        group: payload
      }).then(response => {
        const data = response.data;
        commit('GROUP_SAVE', data.group);
        resolve(response);
      }, error => {
        reject(error);
      })
    });
  },
  groupPermissionSave({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.post('/api/group-permission-save/', {
        group_id: payload.groupId,
        permission_id: payload.permissionId,
        is_checked: payload.isChecked,
      }).then(response => {
        commit('GROUP_PERMISSION_SAVE', payload);
        resolve(response);
      }, error => {
        reject(error);
      })
    });
  },
  userSave({commit}, user) {
    return new Promise((resolve, reject) => {
      const method = user.id ? 'put' : 'post';
      axios[method]('/api/users/save/', {
        user: new UserModel().serialize(user)
      }).then(response => {
        const data = response.data;
        commit('USER_SAVE', new UserModel().deserialize(data.user));
        resolve(data);
      }, error => {
        reject(error);
      })
    });
  },
  userGroupSave({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.post('/api/user-group-save/', {
        user_id: payload.userId,
        group_id: payload.groupId,
        is_checked: payload.isChecked,
      }).then(response => {
        commit('USER_GROUP_SAVE', payload);
        resolve(response);
      }, error => {
        reject(error);
      })
    });
  },
};

const mutations = {
  GROUP_PERMISSIONS_LIST(state, data) {
    state.groups = data.groups;
    state.permissions = data.permissions;
  },
  USER_LIST(state, users) {
    state.users = users.map(user =>
      new UserModel().deserialize(user));
  },
  GROUP_SAVE(state, savedGroup) {
    const groups = state.groups;
    const foundGroup = groups.find(group => group.id === savedGroup.id);
    if (foundGroup) {
      Object.assign(foundGroup, savedGroup);
    } else {
      groups.push(savedGroup);
    }
    state.groups = groups;
  },
  GROUP_PERMISSION_SAVE(state, payload) {
    state.groups = state.groups.map(group => {
      if (group.id === payload.groupId) {
        if (payload.isChecked) {
          group.permissions.push(payload.permissionId)
        } else {
          group.permissions = group.permissions
            .filter(permId => permId !== payload.permissionId)
        }
      }
      return group;
    })
  },
  USER_SAVE(state, savedUser) {
    const users = state.users;
    const foundUser = users.find(user => user.id === savedUser.id);
    if (foundUser) {
      Object.assign(foundUser, savedUser);
    } else {
      users.push(savedUser);
    }
    state.users = users;
  },
  USER_GROUP_SAVE(state, payload) {
    state.users = state.users.map(user => {
      if (user.id === payload.userId) {
        if (payload.isChecked) {
          user.groups.push(payload.groupId)
        } else {
          user.groups = user.groups
            .filter(groupId => groupId !== payload.groupId)
        }
      }
      return user;
    })
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
}
