import axios from "../plugins/axios";
import {getArrayToLocalStorage, setArrayToLocalStorage} from "../functions/common";

const state = {
  brands: [],
  productCategories: [],
  products: [],
  orderProductsInfo: [],
};
const getters = {
  getBrands: (state) => state.brands,
  getOrderProductsInfo: (state) => state.orderProductsInfo,
  getOrderProductsInfoCounts: (state) => {
    console.log('orderProductsInfo', state.orderProductsInfo);
    let length = 0;
    state.orderProductsInfo.map(info => {
      length += info.count
    });
    return length;
  },
  getProductCategories: (state) => state.productCategories,
  getProducts: state => state.products,
  getBrandById: (state) => {
    return (id) => state.brands.find(el => el.id === id)
  },
  getProductCategoryById: (state) => {
    return (id) => state.productCategories.find(el => el.id === id)
  },
  getProductById: (state) => {
    return (id) => state.products.find(el => el.id === id)
  },
};
const actions = {
  fetchBrands({commit}) {
    axios.get('/api/brands/')
      .then(response => {
        commit('BRAND_LIST', response.data);
      })
      .catch(error => console.log(error));
  },
  fetchProductCategories({commit}) {
    axios.get('/api/product-categories/')
      .then(response => {
        commit('PRODUCT_CATEGORY_LIST', response.data);
      })
      .catch(error => console.log(error));
  },
  fetchProducts({commit}) {
    axios.get('/api/products/')
      .then(response => {
        commit('PRODUCT_LIST', response.data);
      })
      .catch(error => console.log(error));
  },
  postBrand({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.post('/api/brands/', payload).then(response => {
        const data = response.data;
        commit('PUSH_BRAND', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  putBrand({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.put('/api/brands/' + payload.id + '/', payload).then(response => {
        const data = response.data;
        commit('UPDATE_BRAND', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  postProductCategory({commit}, payload) {
    return new Promise((resolve, reject) => {
      delete payload.nav_menu_category_obj;
      axios.post('/api/product-categories/', payload).then(response => {
        const data = response.data;
        commit('PUSH_PRODUCT_CATEGORY', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  putProductCategory({commit}, payload) {
    return new Promise((resolve, reject) => {
      delete payload.nav_menu_category_obj;
      axios.put('/api/product-categories/' + payload.id + '/', payload).then(response => {
        const data = response.data;
        commit('UPDATE_PRODUCT_CATEGORY', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  postProduct({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.post('/api/products/', payload).then(response => {
        const data = response.data;
        commit('PUSH_PRODUCT', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  putProduct({commit}, form) {
    return new Promise((resolve, reject) => {
      axios.put('/api/products/' + form.get('id') + '/', form).then(response => {
        const data = response.data;
        commit('UPDATE_PRODUCT', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  postColorImg(_, form) {
    return new Promise((resolve, reject) => {
      axios.post('/api/products/' + 'add-color-img/', form)
        .then(response => {
          const data = response.data;
          resolve(data);
        }, error => { reject(error) })
    });
  },
  deleteColorImg(_, payload) {
    return new Promise((resolve, reject) => {
      axios.post('/api/products/' + 'delete-color-img/', payload)
        .then(response => {
          const data = response.data;
          resolve(data);
        }, error => { reject(error) })
    });
  },
  /* LOCAL ACTIONS */
  fetchOrderProductsInfo({commit}) {
    return new Promise((resolve) => {
      const productsInfo = getArrayToLocalStorage('order.productsInfo');
      commit('SET_PRODUCTS_TO_ORDER', productsInfo);
      resolve(productsInfo);
    });
  },
  addProductToOrder({commit}, selectedInfo) {
    return new Promise((resolve) => {
      const productsInfo = getArrayToLocalStorage('order.productsInfo');
      let isAddProduct = true; // Нужно ли добавлять товар
      if (productsInfo.length) {
        productsInfo.forEach(info => {
          if (selectedInfo.productId === info.productId) { // Если совпадают продукты
            if (selectedInfo.colorIndex) { // Если цвет назначен
              if (selectedInfo.colorIndex === info.colorIndex) { // Если цвета совпадают
                isAddProduct = false; info.count++;
              }
            } else { /* Цвет не назначен */
              isAddProduct = false; info.count++;
            }
          }
        });
      }
      if (isAddProduct) {
        productsInfo.push({
          productId: selectedInfo.productId,
          colorIndex: selectedInfo.colorIndex,
          count: 1,
        });
      }
      commit('ADD_PRODUCT_TO_ORDER', productsInfo);
      setArrayToLocalStorage('order.productsInfo', productsInfo);
      resolve(productsInfo);
    });
  }
};
const mutations = {
  BRAND_LIST(state, brands) { state.brands = brands },
  PRODUCT_CATEGORY_LIST(state, productCategories) { state.productCategories = productCategories },
  PRODUCT_LIST(state, products) { state.products = products; },
  PUSH_BRAND(state, brand) { state.brands.push(brand) },
  UPDATE_BRAND(state, brand) {
    const item = state.brands.find(item => item.id === brand.id);
    if (item) {Object.assign(item, brand) }
  },
  PUSH_PRODUCT_CATEGORY(state, category) { state.productCategories.push(category) },
  UPDATE_PRODUCT_CATEGORY(state, category) {
    const item = state.productCategories.find(item => item.id === category.id);
    if (item) {Object.assign(item, category) }
  },
  PUSH_PRODUCT(state, product) { state.products.push(product) },
  UPDATE_PRODUCT(state, product) {
    const item = state.products.find(item => item.id === product.id);
    if (item) {Object.assign(item, product) }
  },
  /* LOCAL MUTATIONS */
  SET_PRODUCTS_TO_ORDER(state, productsInfo) { state.orderProductsInfo = productsInfo },
  ADD_PRODUCT_TO_ORDER(state, productsInfo) { state.orderProductsInfo = productsInfo },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
