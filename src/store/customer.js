import axios from "../plugins/axios";

const state = {
  productFilters: {
    selectedNavMenu: null
  }
};
const getters = {
  getProductFilters: (state) => state.productFilters,
};
const actions = {
  fetchCustomerInit({ commit }) {
    return new Promise((resolve, reject) => {
      axios.post('/api/users/no-auth-init/')
        .then(response => {
          const data = response.data;
          commit('SITE_INFO', data.site_info);
          commit('NAV_MENU_LIST', data.nav_menu_list);
          commit('BRAND_LIST', data.brands);
          commit('NAV_MENU_CATEGORIES', data.nav_menu_categories);
          commit('PRODUCT_CATEGORY_LIST', data.product_categories);
          resolve(data);
        }, error => { reject(error) })
    });
  },
  fetchProductsByQuery({ commit }, query) {
    return new Promise((resolve, reject) => {
      axios.post('/api/products/by-query/', query)
        .then(response => {
          const data = response.data;
          commit('PRODUCT_LIST', response.data);
          resolve(data);
        }, error => {
          reject(error);
        })
    });
  },
  /* LOCAL ACTIONS */
  setNavMenu({commit}, navMenu) {
    commit('SET_NAV_MENU', navMenu);
  }
};
const mutations = {
  /* LOCAL MUTATIONS */
  SET_NAV_MENU(state, navMenu) { state.productFilters.selectedNavMenu = navMenu },
};

export default {
  state,
  getters,
  actions,
  mutations
};
