import axios from "../plugins/axios";
import site_info_empty from "../models/SiteInfoModel";

const state = {
  site_info: site_info_empty(),
  nav_menu_list: [],
  nav_menu_categories: [],
  leftSidebarDrawer: false,
};
const getters = {
  getLeftSidebarDrawer: state => state.leftSidebarDrawer,
  getSiteInfo: state => state.site_info,
  getNavMenuList: (state) => state.nav_menu_list,
  getNavMenuCategories: (state) => state.nav_menu_categories,
  getNavMenuById: (state) => {
    return (id) => state.nav_menu_list.find(el => el.id === id)
  },
  getNavMenuCategoryById: (state) => {
    return (id) => state.nav_menu_categories.find(el => el.id === id)
  },
};
const actions = {
  async fetchSiteInfo({ commit }) {
    await axios.get('/api/site-info/one/')
      .then(response => {
        commit('SITE_INFO', response.data);
      })
      .catch(error => console.log(error));
  },
  fetchNavMenuList({commit}) {
    axios.get('/api/nav-menu/')
      .then(response => {
        commit('NAV_MENU_LIST', response.data);
      })
      .catch(error => console.log(error));
  },
  fetchNavMenuCategories({commit}) {
    axios.get('/api/nav-menu-categories/')
      .then(response => {
        commit('NAV_MENU_CATEGORIES', response.data);
      })
      .catch(error => console.log(error));
  },
  siteInfoSave({ commit }, siteInfo) {
    return new Promise((resolve, reject) => {
      axios.put('/api/site-info/save/', siteInfo)
        .then(response => {
          const data = response.data;
          commit('SITE_INFO_SAVE', response.data);
          resolve(data);
        }, error => {
          reject(error);
        })
    });
  },
  postNavMenu({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.post('/api/nav-menu/', payload).then(response => {
        const data = response.data;
        commit('PUSH_NAV_MENU', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  putNavMenu({commit}, payload) {
    return new Promise((resolve, reject) => {
      axios.put('/api/nav-menu/' + payload.id + '/', {
        id: payload.id,
        name: payload.name,
        is_active: payload.is_active,
        level_order: payload.level_order,
      }).then(response => {
        const data = response.data;
        commit('UPDATE_NAV_MENU', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  postNavMenuCategory({commit}, payload) {
    return new Promise((resolve, reject) => {
      delete payload.nav_menu_obj;
      axios.post('/api/nav-menu-categories/', payload).then(response => {
        const data = response.data;
        commit('PUSH_NAV_MENU_CATEGORY', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  putNavMenuCategory({commit}, payload) {
    return new Promise((resolve, reject) => {
      delete payload.nav_menu_obj;
      axios.put('/api/nav-menu-categories/' + payload.id + '/', payload).then(response => {
        const data = response.data;
        commit('UPDATE_NAV_MENU_CATEGORY', data);
        resolve(data);
      }, error => { reject(error) })
    });
  },
  /* LOCAL ACTIONS */
  setLeftSidebarDrawer({commit}, drawer) {
    commit('SET_LEFT_SIDEBAR_DRAWER', drawer);
  },
};
const mutations = {
  SITE_INFO(state, site_info) { state.site_info = site_info },
  NAV_MENU_LIST(state, nav_menu_list) { state.nav_menu_list = nav_menu_list; },
  NAV_MENU_CATEGORIES(state, categories) { state.nav_menu_categories = categories; },
  PUSH_NAV_MENU(state, menu) { state.nav_menu_list.push(menu) },
  UPDATE_NAV_MENU(state, menu) {
    const item = state.nav_menu_list.find(item => item.id === menu.id);
    if (item) {Object.assign(item, menu) }
  },
  PUSH_NAV_MENU_CATEGORY(state, category) { state.nav_menu_categories.push(category) },
  UPDATE_NAV_MENU_CATEGORY(state, category) {
    const item = state.nav_menu_categories.find(item => item.id === category.id);
    if (item) {Object.assign(item, category) }
  },
  SITE_INFO_SAVE(state, data) {
    state.site_info = data.site_info ? data.site_info : state.site_info;
  },
  SET_LEFT_SIDEBAR_DRAWER(state, drawer) {
    console.log(1, drawer);
    state.leftSidebarDrawer = drawer;
  },
};

export default {
  state,
  mutations,
  getters,
  actions,
};
