import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import users from './users';
import products from './products';
import common from './common';
import customer from './customer';
import axios, {getJWTBodyData} from "../plugins/axios";
import router from "../router";
import {UserModel} from "../models/UserModel";

export default new Vuex.Store({
  state: {
    isAuthenticated: false,
  },
  getters: {
    isAuthenticated: state => state.isAuthenticated,
  },
  actions: {
    async login({commit}, payload) {
      return await new Promise((resolve, reject) => {
        axios.post('/api/token/', getJWTBodyData(payload))
          .then(response => {
            commit('LOGIN', response.data);
            resolve(response);
          }, error => {
            reject(error);
          })
      });
    },
    logout({commit}) {
      commit('LOGOUT');
      delete axios.defaults.headers.common['Authorization'];
      router.replace({name: 'adminLogin'})
    },
    async checkAuth({commit}) {
      /* При редиректе после login в axios.js в localStorage.token будет null, а здесь уже есть значение */
      axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
      await axios.post('/api/check-auth/')
        .then(response => {
          const data = response.data;
          commit('CHECK_AUTH', data);
          commit('USER_LIST', data.users);
          commit('GROUP_PERMISSIONS_LIST', data);
          commit('BRAND_LIST', data.brands);
          commit('NAV_MENU_CATEGORIES', data.nav_menu_categories);
          commit('PRODUCT_CATEGORY_LIST', data.product_categories);
          commit('PRODUCT_LIST', data.products);
          commit('SITE_INFO', data.site_info);
          commit('NAV_MENU_LIST', data.nav_menu_list);
        })
        .catch(error => {
          console.log(error);
        });
    },
  },
  mutations: {
    LOGIN(state, authInfo) {
      localStorage.setItem('token', authInfo.access);
      localStorage.setItem('refresh', authInfo.refresh);
    },
    LOGOUT(state) {
      localStorage.removeItem('token');
      localStorage.removeItem('refresh');
      state.isAuthenticated = false;
    },
    CHECK_AUTH(state, data) {
      state.isAuthenticated = true;
      state.currentUser = new UserModel().deserialize(data.user);
    },
  },
  modules: {
    users,
    products,
    common,
    customer,
  }
})
