export const ru = {
  main: {
    group: {
      user: 'Пользователи',
      navmenucategory: 'Категория меню',
      productcategory: 'Категория товаров',
      product: 'Товары',
      brand: 'Бренд',
      siteinfo: 'Инфо о сайте',
      navmenu: 'Главное меню',
      permissionAction: {
        add: 'Добавление',
        change: 'Изменение',
        delete: 'Удаление',
        view: 'Просмотр',
      }
    },
    eTableTitle: {
      groups: 'Группы',
      users: 'Пользователи',
      navmenus: 'Меню панели навигации',
      brands: 'Бренды',
      products: 'Товары',
      navmenucategories: 'Категории меню',
      productcategories: 'Категории товаров',
    },
  },
};
