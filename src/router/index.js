import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store';
import AdminMain from '../views/AdminMain.vue';
import AdminLogin from "../views/auth/AdminLogin";
import PageNotFound from "../components/common/PageNotFound";
import DynamicETable from "../views/entitiesContent/eTable/DynamicETable";
import DynamicEAddEdit from "../views/entitiesContent/eAddEdit/DynamicEAddEdit";
import {isNotEmptyObject} from "../functions/is";
import CustomerMain from "../views/customer/CustomerMain";
import Products from "../views/customer/pages/Products";
import Home from "../views/customer/pages/Home";
import SiteInfo from "../views/entitiesContent/eAddEdit/SiteInfo";

Vue.use(VueRouter);

const routes = [
  /* МАРШРУТЫ АДМИНА */
  {
    path: '/admin',
    name: 'adminMain',
    component: AdminMain,
    meta: { isStaff: true },
    children: [
      {
        path: 'e-table/:name',
        name: 'eTable',
        component: DynamicETable,
        meta: { isStaff: true },
      },
      {
        path: 'e-add-edit/:name/:id',
        name: 'eAddEdit',
        component: DynamicEAddEdit,
        meta: { isStaff: true },
      },
      {
        path: 'site-info',
        name: 'siteInfo',
        component: SiteInfo,
        meta: { isStaff: true },
      },
    ]
  },
  {
    path: '/admin/login',
    name: 'adminLogin',
    component: AdminLogin,
    meta: { isStaff: true },
  },
  /* МАРШРУТЫ КЛИЕНТА */
  {
    path: '/',
    component: CustomerMain,
    meta: { isStaff: false },
    children: [
      {
        path: '',
        name: 'customerHome',
        component: Home,
        meta: { isStaff: false },
      },
      {
        path: 'category/:id',
        name: 'category',
        component: Products,
        meta: { isStaff: false },
      },
    ]
  },
  { path: "*", component: PageNotFound }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if ((isNotEmptyObject(to.meta) && to.meta.isStaff)) { // Маршруты админа
    if (to.name === 'adminLogin' || (from.name && from.name !== 'adminLogin')) {
      return next();
    }
    store.dispatch("checkAuth").then(() => {
      if (!store.getters['isAuthenticated']) {
        next({ name: 'adminLogin'});
      } else {
        next();
      }
    })
  } else { // Маршруты клиента
    // TODO авторизация клиента
    return next();
  }
});

export default router
